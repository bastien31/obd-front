// this is the Map component for React!
// import some dependencies
var React = require('react');
var ReactDOM = require('react-dom');
var L = require('leaflet');
require('leaflet/dist/leaflet.css');

// import our subway line filter component
// var Filter = require('./Filter');

// let's store the map configuration properties,
// we could also move this to a separate file & require it...
var config = {};

// map paramaters to pass to L.map when we instantiate it
config.params = {
  center: [43.5669216, 1.3832538], //Greenpoint
  zoomControl: false,
  zoom: 13,
  maxZoom: 18,
  minZoom: 11,
  scrollwheel: true,
  legends: true,
  infoControl: true,
  attributionControl: true
};

// params for the L.tileLayer (aka basemap)
config.tileLayer = {
  uri: 'http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png',
  params: {
    minZoom: 11,
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, &copy; <a href="http://cartodb.com/attributions">CartoDB</a>',
    id: '',
    accessToken: ''
  }
};

// here's the actual component
var ObdMap = React.createClass({
  getInitialState: function() {
    // TODO: if we wanted an initial "state" for our map component we could add it here
    return {
      tileLayer : null,
      geojsonLayer: null,
      geojson: null,
      filter: '*',
      numEntrances: null,
      data: {},
      vin: null,
    };
  },

  // a variable to store our instance of L.map
  map: null,

  componentWillMount: function() {
    // code to run just before adding the map
  },

  getParameterByName: function(name, url) {
      if (!url) url = window.location.href;
      name = name.replace(/[\[\]]/g, "\\$&");
      var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
          results = regex.exec(url);
      if (!results) return null;
      if (!results[2]) return null;
      return decodeURIComponent(results[2].replace(/\+/g, " "));
  },

  componentDidMount: function() {
    this.setState({
      vin: this.getParameterByName('vin')
    });
    console.log("Filter on: " + this.state.vin)

    // code to run just after the component "mounts" / DOM elements are created
    // make the AJAX request for the GeoJSON data
    this.getData();
    // create the Leaflet map object
    if (!this.map) this.init(this.getID());
  },

  componentDidUpdate(prevProps, prevState) {
    // check to see if the subway lines filter has changed
    if (this.state.filter !== prevState.filter) {
      // filter / re-render the map
    }
  },

  componentWillUnmount: function() {
    // code to run just before unmounting the component
    // this destroys the Leaflet map object & related event listeners
    this.map.remove();
  },

  updateMap: function(subwayLine) {
    // change the subway line filter
    if (subwayLine === "All lines") {
      subwayLine = "*";
    }
    // update our state with the new filter value
    this.setState({
      filter: subwayLine
    });
  },

  getData: function() {
      this.connection = new WebSocket('ws://vps311898.ovh.net:9001');
      // this.connection = new WebSocket('ws://127.0.0.1:9001');
      this.connection.onmessage = evt => {
            var receivedData = JSON.parse(evt.data);
            var newPointsLocations = [];
            for (var i=0; i<receivedData.length; i++){
                var point = receivedData[i];
                if (this.state.vin != null && this.state.vin != point.vin){
                  break;
                }

                var geoLoc = [point.location.geoPoint.coordinates[1],
                              point.location.geoPoint.coordinates[0]]
                newPointsLocations.push(geoLoc);

                point.marker = L.circle(geoLoc,
                                         5, {
                                          color: point.overspeed? 'red':'green',
                                          fillColor: point.overspeed? '#f03':'#a4c639',
                                          fillOpacity: 0.5})
                                          .addTo(this.map);
                var value = "vin: " + point.vin + "<br>"
                for (var key in point.obdData){
                    value += key + ":" + point.obdData[key].value + " " + point.obdData[key].unit + "<br>"
                }

                point.marker.bindPopup(value);
            }
            if (newPointsLocations.length > 0){
              this.map.fitBounds(newPointsLocations);
            }
        }
  },

  zoomToFeature: function(target) {
    // pad fitBounds() so features aren't hidden under the Filter UI element
    var fitBoundsParams = {
      paddingTopLeft: [200,10],
      paddingBottomRight: [10,10]
    };
    // set the map's center & zoom so that it fits the geographic extent of the layer
    this.map.fitBounds(target.getBounds(), fitBoundsParams);
  },

  filter: function(feature, layer) {
    // filter the subway entrances based on the map's current search filter
    // returns true only if the filter value matches the value of feature.properties.LINE
    var test = feature.properties.LINE.split('-').indexOf(this.state.filter);

    if (this.state.filter === '*' || test !== -1) {
      return true;
    }
  },

  getID: function() {
    // get the "id" attribute of our component's DOM node
    return ReactDOM.findDOMNode(this).querySelectorAll('#map')[0];
  },

  init: function(id) {
    if (this.map) return;
    // this function creates the Leaflet map object and is called after the Map component mounts
    this.map = L.map(id, config.params);
    L.control.zoom({ position: "bottomleft"}).addTo(this.map);
    L.control.scale({ position: "bottomleft"}).addTo(this.map);

    // a TileLayer is used as the "basemap"
    var tileLayer = L.tileLayer(config.tileLayer.uri, config.tileLayer.params).addTo(this.map);

    // set our state to include the tile layer
    this.setState({
      tileLayer: tileLayer
    });
  },

  render : function() {
    // return our JSX that is rendered to the DOM
    // we pass our Filter component props such as subwayLines array, filter & updateMap methods

    return (
      <div id="mapUI">
        {
          /* render the Filter component only after the subwayLines array has been created */
        //   subwayLines.length ?
        //     <Filter lines={subwayLines} curFilter={this.state.filter} filterLines={this.updateMap} /> :
        //     null
        }
        <div id="map" />
      </div>
    );
  }
});

// export our Map component so that Browserify can include it with other components that require it
module.exports = ObdMap;
